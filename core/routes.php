<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->group('/api', function() {
    $this->get('/categories', 'CategoryController:getAll');
    $this->get('/categories/{id}', 'CategoryController:getById');
    $this->post('/categories', 'CategoryController:create');
    
    $this->get('/courses', 'CourseController:getAll');
    $this->get('/courses/{id}', 'CourseController:getById');
    $this->post('/courses', 'CourseController:create');
});