<?php

namespace App\Models;

use Faker;

class Category extends Model
{
    private $categories = [];

    public function __construct()
    {
        $this->categories = [
            [
                'id' => 'fundamentos',
                'name' => 'Fundamentos',
                'description' => 'Fudamentos básicos do futebol',
                'createdAt' => time()
            ],
            [
                'id' => 'profissional',
                'name' => 'Profissional',
                'description' => 'Habilidades profissionais',
                'createdAt' => time()
            ]
        ];
    }

    public function getAll()
    {
        return $this->categories;
    }

    public function add($id, $name, $description)
    {
        $category = [];
        $category['id'] = $this->getValue($id);
        $category['name'] = $this->getValue($name);
        $category['description'] = $this->getValue($description);
        $category['createdAt'] = time();
        return $category;
    }

    public function getById($id)
    {
        $category = $this->searchForId($id, $this->categories);
        return $category;
    }

}