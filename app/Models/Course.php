<?php

namespace App\Models;

use Faker;

class Course extends Model
{
    private $courses = [];

    public function __construct()
    {
        $this->courses = [
            [
                'id' => 'Ma13jIcb6oQ',
                'image' => 'http://i3.ytimg.com/vi/Ma13jIcb6oQ/maxresdefault.jpg',
                'name' => 'Controle de bola',
                'description' => 'Controle de bola básico para qualquer categoria.',
                'categoryId' => 'fundamentos',
                'createdAt' => time()
            ],
            [
                'id' => 'ZOUKY7p0-c4',
                'image' => 'http://i3.ytimg.com/vi/ZOUKY7p0-c4/maxresdefault.jpg',
                'name' => '3 exercícios para conduzir a bola',
                'description' => 'Condução de bola de 3 maneiras básicas diferentes.',
                'categoryId' => 'fundamentos',
                'createdAt' => time()
            ],
            [
                'id' => 'h0Lx4YBx3QQ',
                'image' => 'http://i3.ytimg.com/vi/h0Lx4YBx3QQ/maxresdefault.jpg',
                'name' => 'Domínio de 3 maneiras diferentes',
                'description' => 'Domínio de bola de 3 maneiras diferentes. Maneiras básicas e importantíssimas.',
                'categoryId' => 'fundamentos',
                'createdAt' => time()
            ],
            [
                'id' => 'Z0nzrKKcJXg',
                'image' => 'http://i3.ytimg.com/vi/h0Lx4YBx3QQ/maxresdefault.jpg',
                'name' => 'Aprenda a marcar',
                'description' => 'Assista tudo e pegue todas as dicas para vc arrebentar',
                'categoryId' => 'profissional',
                'createdAt' => time()
            ],
            [
                'id' => 'DLuPaIfk4U8',
                'image' => 'http://i3.ytimg.com/vi/DLuPaIfk4U8/maxresdefault.jpg',
                'name' => '3 dribles alucinantes',
                'description' => '3 dribles para vocês usarem tanto no futsal, futebol de rua e campo.',
                'categoryId' => 'profissional',
                'createdAt' => time()
            ],
            [
                'id' => '60co76wRDlE',
                'image' => 'http://i3.ytimg.com/vi/60co76wRDlE/maxresdefault.jpg',
                'name' => 'Tenha mais domínio e controle',
                'description' => 'Hoje ensinarei um exercicio muito bom que vai te ajudar a ter aquele dominio de bola.',
                'categoryId' => 'profissional',
                'createdAt' => time()
            ]
        ];
    }

    public function getAll()
    {
        return $this->courses;
    }

    public function add($id, $name, $description, $categoryId)
    {
        $course = [];
        $course['id'] = $this->getValue($id);
        $course['image'] = 'http://i3.ytimg.com/vi/'.$id.'/maxresdefault.jpg';
        $course['name'] = $this->getValue($name);
        $course['description'] = $this->getValue($description);
        $course['categoryId'] = $this->getValue($categoryId);
        $course['createdAt'] = time();
        return $course;
    }

    public function getById($id)
    {
        $course = $this->searchForId($id, $this->courses);
        return $course;
    }

}