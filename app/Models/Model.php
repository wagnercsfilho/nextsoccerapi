<?php

namespace App\Models;

use Faker;

class Model
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }

    public function getValue($value)
    {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }

    protected function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            if ($val['id'] === $id) {
                return $val;
            }
        }
        return null;
    }
}