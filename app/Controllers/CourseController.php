<?php

namespace App\Controllers;

use App\Models\Course;

class CourseController extends Controller
{
    public function getAll($req, $res)
    {
        $course = new Course();
        $courses = $course->getAll();
        return $res->withStatus(200)->withJson($courses);
    }
    
    public function create($req, $res) 
    {
        $course = new Course();
        $id = $req->getParam('id');
        $name = $req->getParam('name');
        $description = $req->getParam('description');
        $categoryId = $req->getParam('categoryId');
        $newCourse = $course->add($id, $name, $description, $categoryId);
        return $res->withStatus(201)->withJson($newCourse);
    }

    public function getById($req, $res) 
    {
        $id = $req->getAttribute('id');
        $course = new Course();
        return $res->withJson($course->getById($id));
    }
}