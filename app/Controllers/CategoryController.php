<?php

namespace App\Controllers;

use App\Models\Category;

class CategoryController extends Controller
{
    public function getAll($req, $res)
    {
        $category = new Category();
        $categories = $category->getAll();
        return $res->withJson($categories);
    }
    
    public function create($req, $res) 
    {
        $category = new Category();
        $id = $req->getParam('id');
        $name = $req->getParam('name');
        $description = $req->getParam('description');
        $newCategory = $category->add($id, $name, $description);
        return $res->withJson($newCategory);
    }

    public function getById($req, $res) 
    {
        $id = $req->getAttribute('id');
        $category = new Category();
        $newCategory = $category->getById($id);
        return $res->withJson($newCategory);
    }
}